// 1. Опишіть своїми словами як працює метод forEach.
// Для перебору елементів: forEach(func) – викликає func для кожного елемента. Нічого не повертає. Метод forEach() виконує функцію callback один раз для кожного елемента, що знаходиться в масиві в порядку зростання. Вона не буде викликана для видалених чи пропущених елементів масиву. Однак, вона буде викликана для елементів, які присутні в масиві і мають значення undefined.


// 2. Як очистити масив?
// Щоб очистити масив від елементів, потрібно встановити його довжину на нуль. Ця операція видаляє всі елементи масиву, і навіть їх значення.


// 3. Як можна перевірити, що та чи інша змінна є масивом?
// Метод Array.isArray() возвращает true, если объект является массивом и false, если он массивом не является.
// Наприклад: Array.isArray(obj), де (obj) являється параметром - об`єктом для перевірки
// Повертається true якщо об`єкт є Array; чи, false.


// function filterBy(array, type) {
//     return array.reduce((res, currentItem) => {
//         if (typeof currentItem != type) {
//             res.push(currentItem);
//         } return res
//     }, []);
// }

function filterBy(array, type) {
    let newArray = []
    newArray = array.filter(el=>typeof el !== type)
    return newArray
}
console.log(filterBy(['Diana', 'Margo', 24, '29', null], 'string'))

